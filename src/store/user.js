import axios from '@/libs/request'
import { setToken, getToken } from '@/libs/util';

export default {
	state: {
		userName: '',
		userId: '',
		avatorImgPath: '',
		token: getToken(),
		access: '',
		hasGetInfo: false
	},
	mutations: {
		setAvator(state, avatorPath) {
			state.avatorImgPath = avatorPath;
		},
		setUserId(state, id) {
			state.userId = id;
		},
		setUserName(state, name) {
			state.userName = name;
		},
		setAccess(state, access) {
			state.access = access;
		},
		setToken(state, token) {
			state.token = token;
			setToken(token);
		},
		setHasGetInfo(state, status) {
			state.hasGetInfo = status;
		}
	},
	getters: {
	
	},
	actions: {
		// 登录
		async handleLogin({ commit }, { userName, password }) {
			return new Promise(resolve => {
				setTimeout(function () {
					commit('setToken', 'QWEDRMNNASIEE897Q5');
					resolve();
				}, 500);
			});
		},
		// 退出登录
		async handleLogOut({ state, commit }) {
			commit('setToken', '');
			commit('setAccess', []);
		},
		// 获取用户相关信息
		async getUserInfo({ state, commit }) {
			return new Promise(resolve => {
				setTimeout(function () {
					let data = {
						access: ["admin"],
						avator: "https://file.iviewui.com/dist/a0e88e83800f138b94d2414621bd9704.png",
						user_id: "14138",
						user_name: "test"
					};
					commit('setAvator', data.avator);
					commit('setUserName', data.name);
					commit('setUserId', data.user_id);
					commit('setAccess', data.access);
					commit('setHasGetInfo', true);
					resolve(data);
				});
			});
		}
	}
}
