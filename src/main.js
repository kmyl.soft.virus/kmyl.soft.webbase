import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';
import iView from 'iview';
import ElementUI from 'element-ui';
import config from '@/config';
import importDirective from '@/directive';
import '@/assets/icons/iconfont.css';
import 'iview/dist/styles/iview.css';
import 'element-ui/lib/theme-chalk/index.css';
import TreeTable from 'tree-table-vue';

Vue.use(iView);
Vue.use(ElementUI);
Vue.use(TreeTable);
/**
 * @description 生产环境关掉提示
 */
Vue.config.productionTip = false;
/**
 * @description 全局注册应用配置
 */
Vue.prototype.$config = config;
/**
 * 注册指令
 */
importDirective(Vue);

new Vue({ el: '#app', router, store, render: h => h(App) });
